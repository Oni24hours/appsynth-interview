import axios from 'axios'

const ACCESS_TOKEN = '2bc8dd8eb18994c20a1ba7f21ba6b2c43df0b7ed'

// Calculate 30 days ago from now
let date = new Date();
date.setDate(date.getDate() - 30);
const DATE_30_DAYS_AGO = date.toISOString().split('T')[0];

const GithubService = {
    // Get Top Star from Github Repositories
    getTopStar(params = {}) {
        const query = `query SearchMostTop10Star($queryString: String!) {
            search(query: $queryString, type: REPOSITORY, first: 10, after: ${params.after}) {
              repositoryCount
              edges {
                node {
                  ... on Repository {
                    name
                    description
                    stargazers {
                      totalCount
                    }
                    forks {
                      totalCount
                    }
                    primaryLanguage {
                      name
                    }
                    updatedAt
                  }
                }
              }
              pageInfo {
                hasNextPage
                endCursor
              }
            }
          }`

        const request = axios.post('https://api.github.com/graphql',
          {
            operationName: 'SearchMostTop10Star',
            query: query,
            variables: {
              queryString: 'created:>' + DATE_30_DAYS_AGO + ' sort:stars-desc'
            }
          },
          {
            headers: {
              Authorization: 'bearer ' + ACCESS_TOKEN,
              'Content-Type': 'application/json'
            }
          })

        return request.then((response) => {
          return response
        }).catch(error => {
          return Promise.reject(error)
        });
    }

}

export default GithubService
