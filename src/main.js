import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import Antd from 'ant-design-vue'
import InfiniteLoading from 'vue-infinite-loading'
import App from './App.vue'
import router from './router'

Vue.config.productionTip = false

Vue.use(VueAxios, axios)
Vue.use(Antd)
Vue.use(InfiniteLoading, { /* options */ })

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
